## About Agency Leads API
Agency Leads is a company providing the contact details of potential employers, and the jobs that they are recruiting for.

## Project Overview
Agency Leads API pulls the job leads details from the Agency Leads API's and look for the primary contact for the job fetched, then it will search for the primary contact in HubSpot contacts. If a primary contact email does not exist in HubSpot. It will create a new contact and associates an engagement in Hubspot with job details as a note body for that contact. If a contact already exists, then It will fetch all the associated engagements and loop through them to check if the Job Note has already been added to the contact or else it will add a new engagement with the job details in the note body.
- - - -

## Installation
```
git clone https://recruitercom.git.beanstalkapp.com/recruiter-com-api-v2-0.git
cd recruiter-com-api-v2-0
composer install
cp .env.example .env
php artisan key:generate
```
Add the required variables values in .env file and you are ready to go.

## Commands
1. php artisan GenerateLeads <limit>
    * This command is used to pull down the provided number of job leads from the API and create the corresponding Hubspot contact if it does not exists, and create engagement node.
    * Limit accepts integer parameter which defines the number of leads to be processed in a batch.
    * Example:  php artisan GenerateLeads 5, here 5 represents limit.
2. php artisan GenerateReport
    * This command is used for generating the daily status report and send a mail to the NOTIFICATION_EMAIL_ID provided in ENV file.
    * This needs to be scheduled once daily.
